using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBehaviourScript : MonoBehaviour
{

    bool tengoQueMoverme = false;
    int rapidez = 4;

    void Update()
    {
        if (transform.position.z >= 2)
        {
            tengoQueMoverme = true;
        }
        if (transform.position.z <= -8.5f)
        {
            tengoQueMoverme = false;
        }

        if (tengoQueMoverme)
        {
            Bajar();
        }
        else
        {
            Subir();
        }

    }

    void Subir()
    {
        transform.position += transform.forward * rapidez * Time.deltaTime;
    }

    void Bajar()
    {
        transform.position -= transform.forward * rapidez * Time.deltaTime;
    }
}