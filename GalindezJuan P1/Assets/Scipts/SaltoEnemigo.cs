using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaltoEnemigo : MonoBehaviour
{
    public Rigidbody rb;
    public CapsuleCollider col;
    public LayerMask capaPiso;
    public float magnitudSalto;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        col = GetComponent<CapsuleCollider>();

    }


    void Update()
    {
        if ( EstaEnPiso())
        {
            rb.AddForce(Vector3.up * magnitudSalto * Time.deltaTime, ForceMode.Impulse);
           

        }
    }
    private bool EstaEnPiso()
    {
        return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x,
        col.bounds.min.y, col.bounds.center.z), col.radius * .9f, capaPiso);
    }
}
