using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ControlJugador : MonoBehaviour
{

    private Rigidbody rb;
    public int rapidez;
    public LayerMask capaPiso;
    public float magnitudSalto;
    public CapsuleCollider col;
    public bool DobleSalto;
    public Text textoCantidadRecolectados;
    public Text textoGanaste;
    private int cont;



    void Start()
    {
        rb = GetComponent<Rigidbody>();
        col = GetComponent<CapsuleCollider>();
        cont = 0;
        textoGanaste.text = "";
        setearTextos();

    }


    private void setearTextos()
    {
        textoCantidadRecolectados.text = "Cantidad recolectados: " + cont.ToString();
        if (cont >= 5)
        {
            textoGanaste.text = "Ganaste!";
        }
    }

    //movimiento
    private void FixedUpdate()
    {
        float movimientoHorizontal = Input.GetAxis("Horizontal");
        float movimientoVertical = Input.GetAxis("Vertical");
        Vector3 vectorMovimiento = new Vector3(movimientoHorizontal, 0.0f, movimientoVertical);
        rb.AddForce(vectorMovimiento * rapidez);
    }

 


        private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("esfera") == true)
        {
            other.gameObject.SetActive(false);
            rapidez += 4;
            transform.localScale = new Vector3(1, 2.5f, 1);
            cont = cont + 1;
            setearTextos();
            other.gameObject.SetActive(false);
        }

        if (other.gameObject.CompareTag("Puntos") == true)
            {
                other.gameObject.SetActive(false);
            cont = cont + 1; setearTextos();
            }

        

    }


    //salto
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && EstaEnPiso())
        {
            rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
            DobleSalto = true;
            
        }

        else if (Input.GetKeyDown(KeyCode.Space) && DobleSalto)
        {
            magnitudSalto += 2;
            rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
            DobleSalto = false;
            magnitudSalto -= 2;
        }




    }

    private bool EstaEnPiso()
    {
        return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x,
        col.bounds.min.y, col.bounds.center.z), col.radius * .9f, capaPiso);
    }









}
