using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class GameManager : MonoBehaviour
{
    public Text Contador;
    float Cronometro;
    public Transform initialPosition;
    public float resetThreshold = -10f;
    public float gameTime = 60f; 
    public Text timerText;
    public Text gameOverText;

    private bool isGameOver = false;

    private Vector3 initialPositionVector;
    private Rigidbody JugadorRigidbody;

    void Start()
    {
        Cronometro = 60;

        initialPositionVector = initialPosition.position;
        JugadorRigidbody = GameObject.FindGameObjectWithTag("Jugador").GetComponent<Rigidbody>();

    }

    void Update()
    {
        Cronometro -= Time.deltaTime;
        Contador.text = "Tiempo restante = " + Cronometro.ToString("f0");

        if (Input.GetKeyDown(KeyCode.R))
        {
            ResetGame();
        }

        if (JugadorRigidbody.transform.position.y < resetThreshold)
        {
            ResetGame();
        }

        if (!isGameOver)
        {
            gameTime -= Time.deltaTime;

            if (gameTime <= 0)
            {
                gameTime = 0;
                GameOver();
            }

            UpdateTimerText();
        }

    }

    private void ResetGame()
    {
        JugadorRigidbody.velocity = Vector3.zero;
        JugadorRigidbody.angularVelocity = Vector3.zero;
        JugadorRigidbody.position = initialPositionVector;

        
    }

    private void GameOver()
    {
        isGameOver = true;
        gameOverText.gameObject.SetActive(true);
        Time.timeScale = 0;
    }

    private void UpdateTimerText()
    {
        timerText.text = "Tiempo: " + Mathf.Ceil(gameTime).ToString();
    }


}


